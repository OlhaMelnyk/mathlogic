﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    AudioSource audioSource;

    [SerializeField] Image soundImage;
    [SerializeField] Sprite soundOn;
    [SerializeField] Sprite soundOff;

    float maxVolume = 1.0f;
    float minVolume = 0.0f;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefs.GetFloat("volume");
        UpdateSoundButton();
    }

    public void SoundButton()
    {
        if(audioSource.volume == maxVolume)
        {
            audioSource.volume = minVolume;
            PlayerPrefs.SetFloat("volume", minVolume);
            UpdateSoundButton();
        }
        else if(audioSource.volume == minVolume)
        {
            audioSource.volume = maxVolume;
            PlayerPrefs.SetFloat("volume", maxVolume);
            UpdateSoundButton();
        }
    }

    public void UpdateSoundButton()
    {
        if (audioSource.volume == minVolume)
        {
            soundImage.sprite = soundOff;
        }
        else if(audioSource.volume == maxVolume)
        {
            soundImage.sprite = soundOn;
        }
    }
}
