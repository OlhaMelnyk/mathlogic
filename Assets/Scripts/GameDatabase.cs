﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;

[CreateAssetMenu(fileName ="GameDatabase", menuName ="Content/GameDatabase",order = 1)]
 public class GameDatabase : ScriptableObject
{
    [SerializeField] List<LevelSettings> levelSettingsList = new List<LevelSettings>();
    public List<LevelSettings> GetLevelSettingList
    {
        get { return levelSettingsList; }
    }
}
