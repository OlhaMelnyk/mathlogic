﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class ButtonsController : MonoBehaviour
{
    private static ButtonsController instance;
    
    [SerializeField] Text inputField;
   
    int number;
    string inputString;

    private void Awake()
    {
        instance = this;
    }

    public void OnButtonPressed()
    {
        string buttonValue = EventSystem.current.currentSelectedGameObject.name;
        inputString += buttonValue;
        int arg;
        if (int.TryParse(inputString, out arg))
        {
            number = arg;
        }
        inputField.text = number.ToString();
    }

    public void OnPressedCheckAnswerButton()
    {
        LevelController.Instance.CheckAnswer(number);
    }

    public static void ClearAnswer()
    {
        instance.inputField.text = "";
        instance.number = 0;
        instance.inputString = "";
    }
}
