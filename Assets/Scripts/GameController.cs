﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    int currentLevel;

    private void Start()
    {
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        if (currentLevel == 0)
        {
            PlayerPrefs.SetInt("currentLevel", 1);
            currentLevel = PlayerPrefs.GetInt("currentLevel");
        }

       LoadGame();
    }
    private void LoadGame()
    {
        UIController.ShowMainGamePanel();
        //LevelButtonController.ChangeColorButton(currentLevel);
        UIController.HideLevelPanel();
        UIController.HideGamePanel();
        UIController.HideWinPanel();
        UIController.HideInformationPanel();
        LevelController.Instance.LoadLevel();
    }

    public void StartGame()
    {
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        LevelButtonController.LoadLevelButtons();
        LevelController.SelectLevel(currentLevel);
    }

    public void RestartGame()
    {
        PlayerPrefs.DeleteKey("currentLevel");
        PlayerPrefs.SetInt("currentLevel", 1);
    }
}
