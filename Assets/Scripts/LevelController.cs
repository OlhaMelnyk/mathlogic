﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class LevelController : MonoBehaviour
{
    private static LevelController instance;

    public static LevelController Instance
    {
        get { return instance; }
    }

    [SerializeField] GameDatabase gameDatabase;
    [SerializeField] GameObject gamePanel;
    [SerializeField] Text currentLevelText;

    GameObject currentTask;
    int indexLevel;
    int currentLevel;
    int nextLevel;

    private void Awake()
    {
        instance = this;
     
    }

    public void LoadLevel()
    {
        UIController.HideWrongAnswerMessage();
      
    }

    public void SetTask(GameObject task)
    {
        currentTask = Instantiate(task, gamePanel.transform);
    }

    public static void DestroyTask()
    {
        Destroy(instance.currentTask);
    }
    

    public static void SelectLevel(int index)
    {
        if (instance.gameDatabase.GetLevelSettingList == null || instance.gameDatabase.GetLevelSettingList.Count == 0)
        {
            Debug.LogError("You must set at least one task");
            return;
        }
        for(int i = 0; i < instance.gameDatabase.GetLevelSettingList.Count; i++)
        {
            if(instance.gameDatabase.GetLevelSettingList[i].id == index)
            {
                instance.SetTask(instance.gameDatabase.GetLevelSettingList[i].task);
                instance.indexLevel = i;
                instance.currentLevel = index;
                Debug.Log(instance.gameDatabase.GetLevelSettingList[i].id);
            }
        }
        UpdatLevelText(instance.currentLevel);
    }

    public static void ChoseNextLevel()
    {
        DestroyTask();
        SelectLevel(instance.nextLevel);
    }

    public void CheckAnswer(int number)
    {
        if (number == instance.gameDatabase.GetLevelSettingList[instance.indexLevel].answer)
        {
            
            UIController.ShowWinPanel();
            UIController.HideGamePanel();
            LevelButtonController.ChangeColorButton(instance.currentLevel);
            UIController.HideWrongAnswerMessage();
            ButtonsController.ClearAnswer();
            

            Debug.Log(instance.currentLevel);
            instance.nextLevel = instance.currentLevel + 1;
            UpdatLevelText(instance.nextLevel);
            if (instance.nextLevel > instance.gameDatabase.GetLevelSettingList.Count)
            {
                UIController.ShowGameOverPanel();
                UIController.HideWinPanel();
                DestroyTask();
            }
            else
            {
                PlayerPrefs.SetInt("currentLevel", instance.currentLevel + 1);
                UIController.HideWrongAnswerMessage();
            }

        }
        else 
        {
            UIController.ShowWrongAnswerMessage();
            Invoke("HideWrongAnswerMessage", 1.0f);
            ButtonsController.ClearAnswer();
        }
    }

    public static void UpdatLevelText(int levelNumber)
    {
        instance.currentLevelText.text = levelNumber.ToString();
    }

    public void HideWrongAnswerMessage()
    {
        UIController.HideWrongAnswerMessage();
        
    }
}
