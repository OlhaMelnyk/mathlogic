﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class UIController : MonoBehaviour
{
    private static UIController instance;

    [Header("References")]
    [SerializeField] GameController gameController;

    [Header("Panels")]
    [SerializeField] GameObject mainGamePanel;
    [SerializeField] GameObject choseLevelPanel;
    [SerializeField] GameObject gamePanel;
    [SerializeField] GameObject winPanel;
    [SerializeField] GameObject gameOverPanel;
    [SerializeField] GameObject informationPanel;

    [SerializeField] Button nextLevel;

    [SerializeField] Text wrongAnswer;

    private void Awake()
    {
        instance = this;
    }

    public void OnPressedStartGameButton()
    {
        gameController.StartGame();
        ShowGamePanel();
        HideMainGamePanel();
       
    }

    public void OnPressedLevelButton()
    {
        HideMainGamePanel();
        LevelButtonController.LoadLevelButtons();
        ShowLevelPanel();
    }

    public void OnPressedGoBackToMenuButton()
    {
        HideLevelPanel();
        ShowMainGamePanel();
    }

    public void OnPressedGoBackToLevelsPanelButton()
    {
        HideGamePanel();
        ShowLevelPanel();
        LevelController.DestroyTask();
        HideWrongAnswerMessage();
    }

    public void OnPressedNextLevelButton()
    {
        LevelController.ChoseNextLevel();
        HideWinPanel();
        ShowGamePanel();
    }

    public void OnPressedGoHomeButton()
    {
        HideGameOverPanel();
        ShowMainGamePanel();
    }

    public void OnPressedRestartButton()
    {
        gameController.RestartGame();
    }

    public static void ShowWinPanel()
    {
        instance.winPanel.SetActive(true);
    }

    public static void HideWinPanel()
    {
        instance.winPanel.SetActive(false);
    }

    public static void ShowMainGamePanel()
    {
        instance.mainGamePanel.SetActive(true);
    }

    public static void HideMainGamePanel()
    {
        instance.mainGamePanel.SetActive(false);
    }

    public static void ShowLevelPanel()
    {
        instance.choseLevelPanel.SetActive(true);
    }

    public static void HideLevelPanel()
    {
        instance.choseLevelPanel.SetActive(false);
    }

    public static void ShowGamePanel()
    {
        instance.gamePanel.SetActive(true);
    }

    public static void HideGamePanel()
    {
        instance.gamePanel.SetActive(false);
    }

    public static void ShowGameOverPanel()
    {
        instance.gameOverPanel.SetActive(true);
    }

    public static void HideGameOverPanel()
    {
        instance.gameOverPanel.SetActive(false);
    }

    public static void ShowWrongAnswerMessage()
    {
        instance.wrongAnswer.gameObject.SetActive(true);
        instance.wrongAnswer.text = "Wrong answer";
    }

    public static void HideWrongAnswerMessage()
    {
        instance.wrongAnswer.gameObject.SetActive(false);
        instance.wrongAnswer.text = " ";
    }

    public void ClearAnswerButton()
    {
        ButtonsController.ClearAnswer();
    }

    public static void ShowInformationPanel()
    {
        instance.informationPanel.SetActive(true);
    }

    public static void HideInformationPanel()
    {
        instance.informationPanel.SetActive(false);
    }

    public void InformationButtonOnPressed()
    {
        ShowInformationPanel();
        HideMainGamePanel();
    }
    public void HideInformationButtonOnPressed()
    {
        HideInformationPanel();
        ShowMainGamePanel();
    }
}
