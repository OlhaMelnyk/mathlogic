﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class LevelButtonController : MonoBehaviour
{
    private static LevelButtonController instance;

    int id;
    int countDoneLevels;
    Button button;

    private void Awake()
    {
        instance = this;
    }

    public static void LoadLevelButtons()
    {
        instance.countDoneLevels = PlayerPrefs.GetInt("currentLevel");
        Debug.Log(instance.countDoneLevels);
        for (int i = 0; i < instance.transform.childCount; i++)
        {
            if (i < instance.countDoneLevels)
            {
                instance.transform.GetChild(i).GetComponent<Button>().image.color = new Color32(169, 169, 169, 255);
                instance.transform.GetChild(i).GetComponent<Button>().interactable = true;
                Debug.Log("oo");
            }
            else
            {
                instance.transform.GetChild(i).GetComponent<Button>().image.color = new Color32(255, 255, 255, 255);
                instance.transform.GetChild(i).GetComponent<Button>().interactable = false;
            }
            
        }
    }

    public void OnButtonPressed()
    {
        UIController.ShowGamePanel();
        UIController.HideLevelPanel();

        int arg;
        if(int.TryParse(EventSystem.current.currentSelectedGameObject.name, out arg)) id = arg;
        button = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        LevelController.SelectLevel(id);
        LevelController.UpdatLevelText(id);
    }

    public static void ChangeColorButton(int index)
    {
         for (int i = 0; i < instance.transform.childCount; i++)
        {
            if (i == index)
            {
                instance.transform.GetChild(i).GetComponent<Button>().image.color = new Color32(169, 169, 169, 255);
                instance.transform.GetChild(i).GetComponent<Button>().interactable = true;
            }
        }
          
    }
}
